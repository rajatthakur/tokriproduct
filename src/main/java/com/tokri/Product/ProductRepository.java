package com.tokri.Product;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    @Query("SELECT a FROM Product a WHERE a.name=:name")
    List<Product> fetchProducts(@Param("name") String name);

    Optional<Product> findById(Long id);
}