package com.tokri.Product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
class ProductDTO {
    @JsonProperty
    Double price;
    @JsonProperty
    private boolean available;

    public ProductDTO(Double price) {
        this.price = price;
    }

    double getPrice() {
        return price;
    }

    boolean isAvailable() {
        return available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDTO that = (ProductDTO) o;
        return Double.compare(that.price, price) == 0 &&
                available == that.available;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, available);
    }
}
