package com.tokri.Product;

class ProductNotCreatedException extends Exception {
    ProductNotCreatedException(String message) {
        super(message);
    }
}
