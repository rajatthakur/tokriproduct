package com.tokri.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ProductsController implements ErrorController {
    @Autowired
    private ProductsService productsService;

    ProductsController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/products")
    Product getProduct(@Valid @RequestParam("id") Long id) throws ProductNotCreatedException {
        return productsService.getProduct(id);
    }

    @PostMapping("/products")
    @ResponseStatus(HttpStatus.CREATED)
    Product create(@Valid @RequestBody Product product) {
        return productsService.add(product);
    }

    @PatchMapping("/products/{id}/availability")
    Product markUnavailable(@PathVariable("id") Long id, @Valid @RequestBody ProductDTO productDTO) throws ProductNotCreatedException {
        return productsService.markUnavailable(id, productDTO.isAvailable());
    }

    @PatchMapping("/products/{id}/price")
    Product updatePrice(@PathVariable("id") Long id, @Valid @RequestBody ProductDTO productDTO) throws ProductNotCreatedException {
        return productsService.updatePrice(id, productDTO.getPrice());
    }

    @RequestMapping("/error")
    public String handleError() {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "<h1>Something went wrong!</h1>\n" +
                "<h2>Our Engineers on it</h2>\n" +
                "</body>\n" +
                "</html>";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
