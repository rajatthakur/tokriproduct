package com.tokri.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class ProductsService {
    public static final String PRODUCT_DOES_NOT_EXIST = "Product does not exist.";
    @Autowired
    private ProductRepository productRepository;

    ProductsService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product add(Product product) {
        return this.productRepository.save(product);
    }

    Product markUnavailable(Long id, Boolean availability) throws ProductNotCreatedException {
        Product product = this.productRepository.findById(id).orElseThrow(() -> new ProductNotCreatedException(PRODUCT_DOES_NOT_EXIST));
        product.setAvailable(availability);
        this.productRepository.save(product);
        return product;
    }

    Product updatePrice(Long id, Double price) throws ProductNotCreatedException {
        Product product = this.productRepository.findById(id).orElseThrow(() -> new ProductNotCreatedException(PRODUCT_DOES_NOT_EXIST));
        product.setPrice(price);
        this.productRepository.save(product);
        return product;
    }

    List<Product> getProducts(String name) {
        return this.productRepository.fetchProducts(name);
    }

    Product getProduct(Long id) throws ProductNotCreatedException {
        return this.productRepository.findById(id).orElseThrow(() -> new ProductNotCreatedException(PRODUCT_DOES_NOT_EXIST));
    }
}

