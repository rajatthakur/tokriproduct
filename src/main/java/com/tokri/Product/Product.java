package com.tokri.Product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

enum Currency {
    DOLLAR, RUPEE;
}

@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
class Product {
    @Id @GeneratedValue
    Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private double price;
    @JsonProperty
    @Enumerated(EnumType.STRING)
    private Currency currency;
    @JsonProperty
    private boolean available;

    public Product(String name, double price, Currency currency, boolean available) {
        this.name = name;
        this.price = price;
        this.currency = currency;
        this.available = available;
    }

    void setPrice(double price) {
        this.price = price;
    }

    void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                available == product.available &&
                Objects.equals(name, product.name) &&
                currency == product.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, currency, available);
    }
}
