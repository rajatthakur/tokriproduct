package com.tokri.Product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
class ProductAvailabilityDTO {
    @JsonProperty
    private boolean available;

    boolean isAvailable() {
        return available;
    }

    public ProductAvailabilityDTO(boolean available) {
        this.available = available;
    }
}
