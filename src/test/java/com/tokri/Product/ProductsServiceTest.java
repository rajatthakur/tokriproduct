package com.tokri.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ProductsServiceTest {
    private ProductsService productsService;

    @Autowired
    private ProductRepository productRepository;

    @BeforeEach
    void setUp() {
        productsService = new ProductsService(productRepository);
    }

    @Test
    void shouldAddProductToRepository() {
        assertEquals(new Product("Banana", 11.0, Currency.DOLLAR, true),
                productsService.add(new Product("Banana", 11.0, Currency.DOLLAR, true)));
    }

    @Test
    void shouldUpdateProductPrice() throws ProductNotCreatedException {
        Product product = productsService.add(new Product("Apple", 11.0, Currency.DOLLAR, true));

        assertEquals(new Product("Apple", 13.0, Currency.DOLLAR, true),
                productsService.updatePrice(product.getId(), 13.0));
    }

    @Test
    void shouldUMakeProductUnavailable() throws ProductNotCreatedException {
        Product product = productsService.add(new Product("Banana", 11.0, Currency.DOLLAR, true));

        assertEquals(new Product("Banana", 11.0, Currency.DOLLAR, false),
                productsService.markUnavailable(product.getId(), false));
    }
}