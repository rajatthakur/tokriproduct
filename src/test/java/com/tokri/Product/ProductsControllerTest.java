package com.tokri.Product;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductsController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
class ProductsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductsService productsService;

    @Test
    void testControllerForFetchingProductWithId() throws Exception {
        when(productsService.getProduct(1L))
                .thenReturn(new Product("Banana", 11.0, Currency.DOLLAR, true));

        String outputString = "{\"id\":null,\"name\":\"Banana\",\"price\":11.0,\"currency\":\"DOLLAR\",\"available\":true}";

        mockMvc.perform(get("/products?id=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(outputString));
    }

    @Test
    void testControllerForAddingAnAvailableProduct() throws Exception {
        when(productsService.add(new Product("Banana", 11.0, Currency.DOLLAR, true)))
                .thenReturn(new Product("Banana", 11.0, Currency.DOLLAR, true));

        String requestString = "{\"name\":\"Banana\",\"price\":11.0,\"currency\":\"DOLLAR\",\"available\":true}";
        String outputString = "{\"id\":null,\"name\":\"Banana\",\"price\":11.0,\"currency\":\"DOLLAR\",\"available\":true}";

        mockMvc.perform(post("/products")
                .content(requestString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(outputString))
                .andDo(document("home"));
    }

    @Test
    void testControllerForMarkingProductUnavailable() throws Exception {
        when(productsService.markUnavailable(1L, false))
                .thenReturn(new Product("Banana", 11.0, Currency.DOLLAR, false));

        String requestString = "{\"available\":false}";
        String outputString = "{\"id\":null,\"name\":\"Banana\",\"price\":11.0,\"currency\":\"DOLLAR\",\"available\":false}";

        mockMvc.perform(patch("/products/1/availability")
                .content(requestString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(outputString));
    }

    @Test
    void testControllerForSpecifyingPriceOfProduct() throws Exception {
        when(productsService.updatePrice(1L, 11.0))
                .thenReturn(new Product("Banana", 11.0, Currency.DOLLAR, false));

        String requestString = "{\"price\":11.0}";
        String outputString = "{\"id\":null,\"name\":\"Banana\",\"price\":11.0,\"currency\":\"DOLLAR\",\"available\":false}";

        mockMvc.perform(patch("/products/1/price")
                .content(requestString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(outputString));
    }
}
