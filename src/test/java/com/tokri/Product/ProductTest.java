package com.tokri.Product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductTest {
    private Product banana;
    private String bananaSerialized;

    @BeforeEach
    void setUp() {
        banana = new Product("Banana", 12.0, Currency.RUPEE, true);
        bananaSerialized = "{\"id\":null,\"name\":\"Banana\",\"price\":12.0,\"currency\":\"RUPEE\",\"available\":true}";
    }

    @Test
    void shouldSerializeUser() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String bananaInJson = mapper.writeValueAsString(banana);

        assertEquals(bananaSerialized, bananaInJson);
    }
}
