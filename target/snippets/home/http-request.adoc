[source,http,options="nowrap"]
----
POST /products HTTP/1.1
Content-Type: application/json
Content-Length: 67
Host: localhost:8080

{"name":"Banana","price":11.0,"currency":"DOLLAR","available":true}
----