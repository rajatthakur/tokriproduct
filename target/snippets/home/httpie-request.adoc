[source,bash]
----
$ echo '{"name":"Banana","price":11.0,"currency":"DOLLAR","available":true}' | http POST 'http://localhost:8080/products' \
    'Content-Type:application/json'
----